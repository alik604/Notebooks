# Notebooks
iPython Notebook folder 

contents 

- Holt, Arima, LinearRegression, and SVM.ipynb 
   + non-ML approach to time series predictions  prediction
- my first LSTM -SPY pred.ipynb
   - code taken [from here](https://colab.research.google.com/drive/1mhsbBTXEwUv9pb1N7MUpFIQ8r73_6-YB#scrollTo=9JnRa0vebWqT). I am learning LSTN for time series predictions by example.  

